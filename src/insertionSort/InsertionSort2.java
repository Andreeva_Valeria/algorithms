package insertionSort;

import java.util.Arrays;
import java.util.List;

/**
 * Класс сортировки значений ArrayList'a по убыванию методом вставки
 *
 * @author Andreeva V.A.
 */
public class InsertionSort2 {
    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList(6, 4, 1, 9, 8, 2, 7, 10, 3, 5);
        System.out.println(integers);
        insertionSort(integers);
        System.out.println(integers);
    }

    /**
     * Сортирует ArrayList'a по убыванию значений методом вставки
     *
     * @param integers ArrayList
     */
    private static void insertionSort(List<Integer> integers) {
        for (int out = 1; out < integers.size(); out++) {
            int temp = integers.get(out);
            int in = out;
            while (in > 0 && integers.get(in - 1) <= temp) {
                integers.set(in, integers.get(in - 1));
                in--;
            }
            integers.set(in, temp);
        }
    }
}
