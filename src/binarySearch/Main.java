package binarySearch;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Метод бинарной сортировки массива и arrayList'a
 *
 * @author Andreeva V.A.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        List<Integer> arrayList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
        System.out.println("Введите число для поиска его индекса в массиве - ");
        int numberInArray = scanner.nextInt();

        if (Binary(array, numberInArray) == -1) {
            System.out.println("Нужного числа нет в массиве ");
        } else {
            System.out.println("Нужный индекс числа найден - " + Binary(array, numberInArray) + " (Massive)");
        }
        if (Binary_Array(arrayList, numberInArray) == -1) {
            System.out.println("Нужного числа нет в массиве ");
        } else {
            System.out.println("Нужный индекс числа найден - " + Binary_Array(arrayList, numberInArray) + " (ArrayList)");
        }

    }

    /**
     * метод бинарной сортировки массива
     *
     * @param array         -упорядоченный массив чисел
     * @param numberInArray - число искомого индекса
     * @return индекс введенного пользователем числа, иначе -1
     */
    private static int Binary(int[] array, int numberInArray) {
        int number = -1;
        int first_index = 0;
        int last_index = array[array.length - 1];
        while (first_index <= last_index) {
            int mid_array = (first_index + last_index) / 2;
            if (array[mid_array] < numberInArray) {
                first_index = mid_array + 1;
            } else if (array[mid_array] > numberInArray) {
                last_index = mid_array - 1;
            } else if (array[mid_array] == numberInArray) {
                number = mid_array;
                break;
            }
            if (numberInArray > array.length) {
                return -1;
            }
        }

        return number;
    }

    /**
     * Метод бинарной сортировки arrayList'a
     *
     * @param arrayList     - упорядоченный массив чисел
     * @param numberInArray - число искомого индекса
     * @return - индекс введенного пользователем числа, иначе -1
     */
    private static int Binary_Array(List<Integer> arrayList, int numberInArray) {
        int number = -1;
        int first_index = 0;
        int last_index = arrayList.get(arrayList.size() - 1);
        while (first_index <= last_index) {
            int mid_array = (first_index + last_index) / 2;
            if (arrayList.get(mid_array) < numberInArray) {
                first_index = mid_array + 1;
            } else if (arrayList.get(mid_array) > numberInArray) {
                last_index = mid_array - 1;
            } else if (arrayList.get(mid_array) == numberInArray) {
                number = mid_array;
                break;
            }
            if (numberInArray > arrayList.size()) {
                return -1;
            }
        }
        return number;
    }
}

