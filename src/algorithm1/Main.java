package algorithm1;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Класс реализации алгоритма последовательного поиска числа среди эл-тов массива и ArrayList'а
 *
 * @author Andreeva V.A.
 */
public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] massive = {4, 3, 7, 5, 9, 13, 87, 69, 34};
        List<Integer> integers = Arrays.asList(5, 4, 18, 76, 3, 0);
        System.out.println("Массив элементов: ");
        for (int value : massive) {
            System.out.print(value + " ");
        }
        System.out.println();
        System.out.println("ArrayList элементов: ");
        for (Integer integer : integers) {
            System.out.print(integer + " ");
        }
        System.out.println();

        System.out.print("Введите целое число: ");
        final int number = scanner.nextInt();


        int index = searchIndexInMas(massive, number);
        printIndex(index);

        index = searchIndexInArr(integers, number);
        printIndex(index);

    }

    /**
     * Выводит информацию в зависимости от рез-тов поиска
     *
     * @param index индекс эл-та
     */
    private static void printIndex(int index) {
        if (index == -1) {
            System.out.println("Такого числа нет ");
        } else {
            System.out.println("Ваше число находится под индексом " + index);
        }
    }

    /**
     * Возвращает индекс эл-та ArrayList'а, если число найдено, иначе -1
     *
     * @param integers ArrayList
     * @param number   число
     * @return индекс эл-та ArrayList, если число найдено, иначе -1
     */
    private static int searchIndexInArr(List<Integer> integers, int number) {
        int index = -1;
        for (int i = 0; i < integers.size(); i++) {
            if (integers.get(i) == number) {
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     * Возвращает индекс эл-та массива, если число найдено, иначе -1
     *
     * @param massive массив
     * @param number  число
     * @return индекс эл-та массива, если число найдено, иначе -1
     */
    private static int searchIndexInMas(int[] massive, int number) {
        int index = -1;
        for (int i = 0; i < massive.length; i++) {
            if (massive[i] == number) {
                index = i;
                break;
            }
        }
        return index;
    }
}
