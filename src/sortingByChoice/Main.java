package sortingByChoice;

import java.util.Arrays;
import java.util.List;

/**
 * Класс сортировки выбором массива и ArrayList'a целых чисел
 *
 * @author Andreeva V.A.
 */
public class Main {
    public static void main(String[] args) {
        int[] massive = {10, 3, 7, 1, 2, 8, 9, 5, 4, 6};
        List<Integer> integers = Arrays.asList(6, 4, 1, 9, 8, 2, 7, 10, 3, 5);

        printElements(massive, integers);

        sortingByChoice(massive);
        sortingByChoice2(integers);

        printElements(massive, integers);
    }

    /**
     * Выводит элементы массива и ArrayList'a
     *
     * @param massive  массив
     * @param integers ArrayList
     */
    private static void printElements(int[] massive, List<Integer> integers) {
        System.out.println(Arrays.toString(massive));
        System.out.println(integers);

    }

    /**
     * Сортирует массив целых чисел методом сортировки выбором
     *
     * @param massive массив для сортировки целых чисел
     */
    private static void sortingByChoice(int[] massive) {
        for (int i = 0; i < massive.length - 1; i++) {
            int minI = i;
            for (int j = i + 1; j < massive.length; j++) {
                if (massive[minI] > massive[j]) {
                    minI = j;
                }
            }
            int t = massive[i];
            massive[i] = massive[minI];
            massive[minI] = t;
        }
    }

    /**
     * Сортирует ArrayList целых чисел методом сортировки выбором
     *
     * @param integers ArrayList
     */
    private static void sortingByChoice2(List<Integer> integers) {
        int t;
        for (int i = 0; i < integers.size() - 1; i++) {
            for (int j = i + 1; j < integers.size(); j++) {
                if (integers.get(j) < integers.get(i)) {
                    t = integers.get(i);
                    integers.set(i, integers.get(j));
                    integers.set(j, t);
                }
            }
        }
    }
}
