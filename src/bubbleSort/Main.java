package bubbleSort;

import java.util.Arrays;

/**
 * Класс пузырьковой сортировки массива целых чисел
 *
 * @author Andreeva V.A
 */
public class Main {
    public static void main(String[] args) {
        int[] massive = {2, 9, 7, 3, 6, 4, 1, 5, 8};
        System.out.println(Arrays.toString(massive));
        bubble(massive);
        System.out.println(Arrays.toString(massive));
    }

    /**
     * Сортирует массив целых чисел методом пузырьковой сортировки
     *
     * @param massive массив целых чисел
     */
    private static void bubble(int[] massive) {
        boolean flag = false;
        while (!flag) {
            flag = true;
            for (int i = 0; i < massive.length; i++) {
                int t;
                for (int j = i + 1; j < massive.length-1; j++) {
                    if (massive[j] > massive[i]) {
                        flag = false;

                        t = massive[i];
                        massive[i] = massive[j];
                        massive[j] = t;
                    }
                }
            }
        }
    }
}
