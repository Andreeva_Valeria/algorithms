package bubbleSort;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Класс пузырьковой сортировки ArrayList'a целых чисел
 *
 * @author Andreeva V.A.
 */
public class BubbleSort2 {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(3, 5, 2, 1, 10, 4, 9, 8, 7, 6));

        System.out.println(arrayList);
        bubbleSort(arrayList);
        System.out.println(arrayList);
    }

    /**
     * Сортирует ArrayList целых чисел методом пузырьковой сортировки
     *
     * @param arrayList ArrayList целых чисел
     */
    private static void bubbleSort(ArrayList<Integer> arrayList) {
        for (int i = arrayList.size() - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arrayList.get(j) > arrayList.get(j + 1)) {
                    int temp = arrayList.get(j);
                    arrayList.set(j, arrayList.get(j + 1));
                    arrayList.set(j + 1, temp);
                }
            }
        }
    }
}


