package quickSort;

import java.util.Arrays;

/**
 * Метод быстрой сортировки заданного массива
 *
 * @author Andreeva V.A.
 */
public class Main {
    public static void main(String[] args) {
        int[] array = {8, 2, 4, 1, 6, 5, 9, 7, 3};
        int arrayOfLessNumbers = 0;
        int arrayOfLargerNumbers = array.length - 1;

        System.out.println(Arrays.toString(array));

        quickSort(array, arrayOfLessNumbers, arrayOfLargerNumbers);

        System.out.println(Arrays.toString(array));
    }

    /**
     * Метод быстрой сортировки заданного массива
     *
     * @param array  заданный массив
     * @param arrayOfLessNumbers   подмассив чисел,которые меньше опорного
     * @param arrayOfLargerNumbers подмассив чисел,которые больше опорного
     */
    private static void quickSort(int[] array, int arrayOfLessNumbers, int arrayOfLargerNumbers) {
        if (array.length < 2) { // Если в < 2 чисел, то сортировка не имеет смысла
            System.out.println("Недостаточно чисел для сортировки");
        } else {
            int middleNumber = arrayOfLessNumbers + (arrayOfLargerNumbers - arrayOfLessNumbers) / 2; // выбор опорного элемента
            int supportElement = array[middleNumber];

            int x = arrayOfLessNumbers, y = arrayOfLargerNumbers; //разделение на 2 подмассива: числа меньше опорного числа и числа больше опорного числа
            while (x <= y) {
                while (array[x] < supportElement) {
                    x++;
                }

                while (array[y] > supportElement) {
                    y--;
                }

                if (x <= y) {
                    int temp = array[x];
                    array[x] = array[y];
                    array[y] = temp;
                    x++;
                    y--;
                }
            }

            if (arrayOfLessNumbers < y)//рекурсия для сортировки массива чисел меньше опорного
                quickSort(array, arrayOfLessNumbers, y);

            if (arrayOfLargerNumbers > x)//рекурсия для сортировки массива чисел больше опорного
                quickSort(array, x, arrayOfLargerNumbers);
        }
    }
}


