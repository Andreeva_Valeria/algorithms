package numbersOfFibonacci;

import java.util.Scanner;

/**
 * Программа для поиска числа Фибоначи
 *
 * @author Andreeva V.A.
 */

public class Main {
    public static void main(String[] args) {
        long before = System.currentTimeMillis();
        long f1 = 1;
        long f2 = 1;
        long a = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число Фибоначи,которое хотите найти - ");
        long number = scanner.nextLong();
        long x = numbersOfFibonacci(f1, f2, number, a);
        System.out.println("Число Фибоначи под " + number + " номером - " + x);
        long after = System.currentTimeMillis();
        System.out.println(after - before + " миллисекунд");
        fibonacci(number);

    }

    /**
     * Вычисляет и возвращает число Фибоначи под номером введенным пользователем
     *
     * @param f1     первое число
     * @param f2     второе число
     * @param number вводимое пользователем число,которое метод должен найти
     * @param a      переменная,куда сохраняются вычисления
     * @return число под номером введенным пользователем
     */
    private static long numbersOfFibonacci(long f1, long f2, long number, long a) {
        for (int i = 3; i <= number; i++) {
            a = f1 + f2;
            f1 = f2;
            f2 = a;
        }
        return a;
    }

    /**
     * Метод нахождения n-го числа фибоначчи рекурсивным способом
     *
     * @param number число заданное с консоли
     * @return number число фибоначчи
     */
    private static int fibonacci(long number) {
        if (number == 0) {
            return 0;
        } else if (number == 1) {
            return 1;
        } else {
            return fibonacci(number - 1) + fibonacci(number - 2);
        }
    }
}

