package objectSort;

public class Person {
    private String surname;
    private int birthday;

    String getSurname() {
        return surname;
    }

    int getBirthday() {
        return birthday;
    }

    Person(String surname, int birthday) {
        this.surname = surname;
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Персона {" +
                "Фамилия='" + surname + '\'' +
                ", Год рождения=" + birthday +
                '}';
    }
}
