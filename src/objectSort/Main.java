package objectSort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Метод сортировки объекта
 *
 * @author Andreeva V.A.
 */
public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("Абрамов", 1971);
        Person person2 = new Person("Беседин", 1994);
        Person person3 = new Person("Вологодин", 1955);
        Person person4 = new Person("Галькин", 1974);
        Person person5 = new Person("Досишкин", 1999);

        ArrayList<Person> arrayList = new ArrayList<>(Arrays.asList(person1, person2, person3, person4, person5));
        ArrayList<Person> sameSurname = new ArrayList<>();

        surnameSorting(arrayList);
        print(arrayList);

        birthYearSorting(arrayList);
        print(sameSurname);

        sameSurname(arrayList, sameSurname);
        println(sameSurname);
    }

    /**
     * Сортирует arrayList по фамилиям
     *
     * @param arrayList список персон
     */
    private static void surnameSorting(ArrayList<Person> arrayList) {
        Person temp;
        for (int i = 0; i < arrayList.size() - 1; i++) {
            for (int j = 0; j < arrayList.size(); j++) {

                int compare = arrayList.get(j).getSurname().compareTo(arrayList.get(i).getSurname());
                if (compare < 0) {
                    temp = arrayList.get(i);
                    arrayList.set(i, arrayList.get(j));
                    arrayList.set(j, temp);
                }
                if (compare > 0) {
                    temp = arrayList.get(i);
                    arrayList.set(i, arrayList.get(j));
                    arrayList.set(j, temp);
                }
            }
        }
    }

    /**
     * Сортирует элементы arrayList'a по дате рождения
     *
     * @param arrayList список персон
     */
    private static void birthYearSorting(ArrayList<Person> arrayList) {
        Person temp;

        for (int i = 0; i < arrayList.size() - 1; i++) {
            for (int j = 0; j < arrayList.size(); j++) {
                if (arrayList.get(i).getBirthday() > arrayList.get(i).getBirthday()) {
                    temp = arrayList.get(i);
                    arrayList.set(i, arrayList.get(j));
                    arrayList.set(j, temp);
                }
            }
        }
    }

    /**
     * Находит однофамильцев в списке
     *
     * @param arrayList   список персон
     * @param sameSurname фамилия,вводимая пользователем
     */
    private static void sameSurname(ArrayList<Person> arrayList, ArrayList<Person> sameSurname) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите фамилию: ");
        String surname = scanner.nextLine();
        for (Person person : arrayList) {
            if (surname.equals(person.getSurname())) {
                sameSurname.add(person);
            }
        }
    }

    /**
     * Выводит элементы arrayList'a
     *
     * @param arrayList
     */
    private static void print(ArrayList<Person> arrayList) {
        for (Person person : arrayList) {
            System.out.println(person);
        }
        System.out.println();
    }

    /**
     * вывод отсортированного списка персон
     *
     * @param sameSurname списко персон
     */
    private static void println(ArrayList<Person> sameSurname) {
        if (sameSurname.isEmpty()) {
            System.out.println("Таких фамилий нет в списке");
        } else {
            for (Person person : sameSurname) {
                System.out.println(person + " ");
            }
        }
    }
}
